---
title: "Deploy Image to Gitlab Registry"
date: 2019-01-22T11:29:00+01:00
anchor: "deployment-gitlab-registry"
---

Instead of [deploying an image of the application to Docker Hub]({{< ref
"deployment-docker-hub.md" >}}), there is also the possibility to directly use
the [Gitlab
Registry](https://docs.gitlab.com/ee/user/project/container_registry.html)
which is accessible via the Gitlab UI. It's even easier to set up than an
automatic push to Docker Hub, since you don't have to define additional
variables. In fact variables are used, but they are all already [baked in](https://gitlab.com/help/ci/variables/README#predefined-variables-environment-variables) the
Runner environment.

## CI configuration file

```yaml
build-image:
  image: docker:stable
  services:
    - docker:dind
  script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
    - docker build -t "$CI_REGISTRY_IMAGE" .
    - docker push "$CI_REGISTRY_IMAGE"
    - docker logout
```

`CI_REGISTRY_USER` and `CI_REGISTRY_PASSWORD` provide the needed
authentication tokens to login into the registry. `CI_REGISTRY` is the name of
the registry, ie. `registry.gitlab.com`, while `CI_REGISTRY_IMAGE` is the
entire address of the image in the registry.

## Using the image

In order to use the image, navigate to the `Registry` page for the
respective repository. There you'll find the coordinates for pulling the
image.

{{% notice note %}}
If your repository is private, you have to login first via

```bash
docker login -u <username> -p <password> registry.gitlab.com
```

or, if you enabled [two-factor authentication](https://gitlab.com/help/user/profile/account/two_factor_authentication), with an [access
token](https://gitlab.com/help/user/profile/personal_access_tokens.md).
{{% /notice %}}

## Automatic alert of new Docker image

{{% notice note %}}
This paragraph is only tentative. Further investigation is needed.
{{% /notice %}}

It should be possible to perform an automatic alert to a deploy target (e.g.
an environment for testing) whenever a new image has been created.
Leverage [webhooks](https://gitlab.com/help/user/project/integrations/webhooks), especially the ones for job (no documentation?) and [pipeline events](https://gitlab.com/help/user/project/integrations/webhooks#pipeline-events).
