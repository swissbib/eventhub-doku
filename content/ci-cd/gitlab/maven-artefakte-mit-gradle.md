---
title: "Maven Artefakte auf Gitlab mit Gradle erstellen"
date: 2019-02-26T17:56:00+01:00
anchor: "maven-artefakte-mit-gradle"
---

Gitlab bietet die Möglichkeit an, Maven-Artefakte von Java-Applikationen
direkt in einem projektspezifischen Maven-Repository zur Verfügung zu stellen.
Das entsprechende Setup ist in der Gitlab-Dokumentation
[beschrieben](https://gitlab.com/help/user/project/packages/maven_repository.html).
Ein analoges Verfahren ist auch bei der Verwendung von Gradle als Build-Tool
möglich. Dazu muss einerseits `gradle.build`, anderseits `.gitlab-ci.yml`
angepasst werden. 

## gradle.build

In `gradle.build` wird konfiguriert,

* was,
* wohin und
* wie

publiziert werden soll.

In unserem Fall wollen wir ein Maven Artefakt in ein Maven Repository laden,
wofür wir das Gradle-Plugin [`maven-publish`](https://docs.gradle.org/current/userguide/publishing_maven.html) benötigen:

```groovy
plugins {
  id 'maven-publish'
}
```

Das Plugin implementiert das Interface [`PublishingExtension`](https://docs.gradle.org/current/javadoc/org/gradle/api/publish/PublishingExtension.html), welche die Methoden [`publications`](https://docs.gradle.org/current/javadoc/org/gradle/api/publish/PublishingExtension.html#publications-org.gradle.api.Action-) und [`repositories`](https://docs.gradle.org/current/javadoc/org/gradle/api/publish/PublishingExtension.html#repositories-org.gradle.api.Action-) definiert.

* In der `publications`-Methode wird die "Publikation" konfiguriert, welche
  in das Repository geladen wird. Das `maven-publish`-Plugin stellt dafür den
Typ [`MavenPublication`](https://docs.gradle.org/current/dsl/org.gradle.api.publish.maven.MavenPublication.html) zur Verfügung
* Handkehrum werden in der `repositories`-Methode die Koordination des
  Ziel-Repository festgehalten. Das Plugin bietet dafür den Typ [`MavenArtifactRepository`](https://docs.gradle.org/current/dsl/org.gradle.api.artifacts.repositories.MavenArtifactRepository.html) an

### `MavenPublication`

`MavenPublication` stellt eine Reihe von Methoden zur Verfügung:

* [`from`](https://docs.gradle.org/current/javadoc/org/gradle/api/publish/maven/MavenPublication.html#from-org.gradle.api.component.SoftwareComponent-): [Komponente](https://docs.gradle.org/current/userguide/publishing_overview.html#glossary:component), welche publiziert werden soll, bspw. [`components.java`](https://docs.gradle.org/current/userguide/java_plugin.html#sec:java_plugin_publishing)
* [`artifact`](https://docs.gradle.org/current/javadoc/org/gradle/api/publish/maven/MavenPublication.html#artifact-java.lang.Object-): Weitere Artefakte (wie bspw. ein durch das [shadow](https://github.com/johnrengelman/shadow)-Plugin gebildetes "Fat JAR")
* Artefakt-Metadaten ([`groupId`](https://docs.gradle.org/current/javadoc/org/gradle/api/publish/maven/MavenPublication.html#setGroupId-java.lang.String-), [`artifactId`](https://docs.gradle.org/current/javadoc/org/gradle/api/publish/maven/MavenPublication.html#setArtifactId-java.lang.String-), [`version`](https://docs.gradle.org/current/javadoc/org/gradle/api/publish/maven/MavenPublication.html#setVersion-java.lang.String-))
* [`pom`](https://docs.gradle.org/current/javadoc/org/gradle/api/publish/maven/MavenPublication.html#pom-org.gradle.api.Action-): Weitere POM-Koordinaten

Ein ausführliches Beispiel von der `maven-publish`-Website:

```groovy
apply plugin: "java"
apply plugin: "maven-publish"

task sourceJar(type: Jar) {
  from sourceSets.main.allJava
  classifier "sources"
}

publishing {
  publications {
    myPublication(MavenPublication) {
      from components.java
      artifact sourceJar
      pom {
        name = "Demo"
        description = "A demonstration of Maven POM customization"
        url = "http://www.example.com/project"
        licenses {
          license {
            name = "The Apache License, Version 2.0"
            url = "http://www.apache.org/licenses/LICENSE-2.0.txt"
          }
        }
        developers {
          developer {
            id = "johnd"
            name = "John Doe"
            email = "john.doe@example.com"
          }
        }
        scm {
          connection = "scm:svn:http://subversion.example.com/svn/project/trunk/"
          developerConnection = "scm:svn:https://subversion.example.com/svn/project/trunk/"
          url = "http://subversion.example.com/svn/project/trunk/"
        }
      }
    }
  }
}
```

### `MavenArtifactRepository`

Über das `MavenArtifactRepository` wird ein Maven-Repository definiert. Das
Interface umfasst folgende Methoden:

* [`name`](https://docs.gradle.org/current/dsl/org.gradle.api.artifacts.repositories.MavenArtifactRepository.html#org.gradle.api.artifacts.repositories.MavenArtifactRepository:name): Der Name des Repository
* [`url`](https://docs.gradle.org/current/dsl/org.gradle.api.artifacts.repositories.MavenArtifactRepository.html#org.gradle.api.artifacts.repositories.MavenArtifactRepository:url): Die Basis-URL des Repository, welche gebraucht wird, um das POM und die Artefakt-Dateien zu finden.
* [`authentication`](https://docs.gradle.org/current/dsl/org.gradle.api.artifacts.repositories.MavenArtifactRepository.html#org.gradle.api.artifacts.repositories.MavenArtifactRepository:authentication): Das Authentifizierungsschema für das Repository. Für Implementierungen s. das [`Authentication`](https://docs.gradle.org/current/dsl/org.gradle.authentication.Authentication.html)-Interface.
* [`credentials`](https://docs.gradle.org/current/dsl/org.gradle.api.artifacts.repositories.MavenArtifactRepository.html#org.gradle.api.artifacts.repositories.MavenArtifactRepository:credentials): Die Credentials zur Authentifizierung. Für Implementierungen s. das [`Credentials`](https://docs.gradle.org/current/javadoc/org/gradle/api/credentials/Credentials.html)-Interface.
* [`artifactUrls`](https://docs.gradle.org/current/dsl/org.gradle.api.artifacts.repositories.MavenArtifactRepository.html#org.gradle.api.artifacts.repositories.MavenArtifactRepository:artifactUrls):	URLs für zusätzliche Artefakt-Dateien

### Konfiguration für Gitlab

```groovy
publishing {
  publications {
    mavenJava(MavenPublication) {
      from components.java // JAR-Datei mit Kompilat
      artifact shadowJar   // Fat JAR
    }
  }
  
  repositories {
    maven {
      name "gitlab-maven"
      url 'https://gitlab.com/api/v4/projects/' + System.getenv("CI_PROJECT_ID") + '/packages/maven'
      credentials(HttpHeaderCredentials) {
        name = "Job-Token"
        value = System.getenv("CI_JOB_TOKEN")
      }
      authentication {
        header(HttpHeaderAuthentication)
      }
    }
  }
}
```

Da der Build im Gitlab-CI-Workflow geschieht, kann zur Laufzeit
praktischerweise auch auf Umgebungsvariablen zugegriffen werden, welche
entweder in `.gitlab-ci.yml` oder
[global](https://docs.gitlab.com/ee/ci/variables/#predefined-environment-variables) definiert sind. Damit lässt sich u.a., wie im Beispiel ersichtlich, die ID des Repositories oder das für die Authentifizierung gegenüber Repository-spezifischen Services wie der Registry benötigte `JOB_TOKEN` erfragen.

## gitlab-ci.yml

Der Publizierungstask wird mit `gradle publish` ausgelöst. In
`.gitlab-ci.yml` kann ein entsprechender Job dann bspw. folgendermassen aussehen:

```yaml
deploy-package:
  image: java:8-jdk
  script:
    - ./gradlew publish
```
