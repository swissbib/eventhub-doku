---
title: "Integration Testing in Gitlab CI"
date: 2019-01-24T08:46:00+01:00
anchor: "integration-testing-gitlab"
---

Der Gitlab CI-Workflow stellt verschiedene Möglichkeiten zur Verfügung,
Integration Tests für Applikationen aufzusetzen:

* Durch die Verwendung von mehreren Docker-Images
* Durch einen Docker-in-Docker-Ansatz
* Mittels des Build-Tools kaniko

Ich möchte hier meine Erfahrungen mit den ersten beiden Methoden festhalten.
Der dritte Ansatz mit kaniko wird [hier](https://docs.gitlab.com/ee/ci/docker/using_kaniko.html) genauer erläutert.

## Problemstellung

Mit Hilfe des `image`-Schlüsselwortes kann in `.gitlab-ci.yml` das
Docker-Image spezifiziert werden, in welchem der CI-Workflow ausgeführt werden
soll. Für _Integration Tests_, d.h. Testverfahren, welche die korrekte
Interaktion der gesamten Applikation mit weiteren Services (z.B. Datenbanken)
überprüft, genügt dies in der Regel nicht. Zwar ist es grundsätzlich denkbar,
die benötigten Services direkt in einem geeigneten Docker-Image zu erstellen,
doch ist dies ressourcenintensiv, möglicherweise mit vielen
Konfigurationseinstellungen verbunden und, insbesondere in einer
Microservice-Architektur, auch nicht sehr realitätsnahe. Deshalb ist es von
Vorteil, die Services jeweils in ihrem eigenen Docker-Container laufen zu
lassen.

## Mehrere Docker-Images

In `.gitlab-ci.yml` lassen sich direkt weitere Docker-Images einbinden, die
vom Runner im CI-Workflow __vor__ dem Haupt-Image aufgestartet und als
Container in ein gemeinsames Docker-Netzwerk eingebunden werden. Dies
geschieht mittels eines
`services`-Blocks:

```yaml
test:
  image: base/archlinux
  services:
    - docker.elastic.co/elasticsearch/elasticsearch:5.4.3
    - neo4j:latest
  script:
    - curl 'docker.elastic.co-elasticsearch-elasticsearch:9200'
    - curl neo4j:7474
```

### `alias`

Im gemeinsamen Netzwerk können die Services unter ihrem Dockernamen gefunden
werden, wobei `/` durch `-` oder `__` ersetzt werden müssen. Dies ist bei
längeren Namen (wie bei dem obigen Elasticsearch-Image) natürlich etwas
umständlich. Aus diesem Grund ist es möglich, Services ein Alias zu geben:

```yaml
# ...
  services:
    - name: docker.elastic.co/elasticsearch/elasticsearch:5.4.3
      alias: es
# ...
  script:
    - curl es:9200
# ...
```

{{% notice info %}}
Werden weitere Konfigurationen für ein Service definiert, muss der Name des
Docker-Images mit dem Tag `name` definiert werden.
{{% /notice %}}

### `entrypoint` und `command`

Der Request auf Elasticsearch wird einen Fehler zurückgeben, da im verwendeten
Image standardmässig eine Authentifizierung notwendig ist. Um den
Elasticsearch-Container ohne Zugangskontrolle zu starten, müssen wir die
Einstellung `xpack.security.enabled=false` setzen.

Der einfachste Weg wäre, wenn diese Einstellung via Umgebungsvariable
mitgegeben werden könnte. Diese können im [`variables`](https://docs.gitlab.com/ee/ci/yaml/README.html#variables)-Schlüsselwort in
`.gitlab-ci.yml` definiert werden:

```yaml
test:
  variables:
    - EINE_UMGEBUNGSVARIABLE: "der_inhalt"
# ...
```

{{% notice note %}}
Umgebungsvariablen werden jeweils für den ganzen CI-Workflow oder den
einzelnen Job definiert und können nicht für einen einzelnen Service gesetzt
werden.
{{% /notice %}}

Allerdings liest das verwendete Elasticsearch-Image keine Umgebungsvariablen
aus, sondern [kann nur folgendermassen konfiguriert werden](https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html#docker-configuration-methods):

* Über die Kommandozeile beim Aufstarten des Containers
* Konfigurationsdatei einbinden
* Image individualisieren
* Die `CMD`-Optionen überschreiben

Die ersten zwei Möglichkeiten sind nicht umsetzbar, da der Container vom
Runner automatisch aufgestartet wird. Das dritte wäre zwar möglich (s.u. zu
Docker-in-Docker), aber aufwendig. Bleibt schliesslich die vierte Option, die
dank den weiteren Konfigurationsmöglichkeiten `entrypoint` und `command`
tatsächlich machbar ist:

* `entrypoint` überschreibt
  [`ENTRYPOINT`](https://docs.docker.com/engine/reference/builder/#entrypoint)
des Docker-Images
* `command` überschreibt
  [`CMD`](https://docs.docker.com/engine/reference/builder/#cmd) des
Docker-Images

In unserem Beispiel enden wir schliesslich bei folgender Konfiguration:

```yaml
test:
  image: base/archlinux
  services:
    - name: docker.elastic.co/elasticsearch/elasticsearch:5.4.3
      alias: es
      command: [ "bin/elasticsearch", "-Expack.security.enabled=false", "-Ediscovery.type=single-node" ]
    - neo4j:latest
  script:
    - curl es:9200
    - curl neo4j:7474
```

{{% notice info %}}
Die Einstellung `discovery.type=single-node` ist notwendig, wenn sich ein
Client via TCP verbinden möchte (i.d.R. über Port `9300`).
{{% /notice %}}


## Docker-in-Docker

Durch die Möglichkeit, `CMD` und `ENTRYPOINT` in services zu überschreiben,
bietet dieser Ansatz zwar schon eine Menge Flexibilität. Ist aber das
Aufsetzen einer komplexen Infrastruktur notwendig, oder soll die
Docker-Infrastruktur gar mit `docker-compose` gesteuert werden, kommt
Docker-in-Docker ins Spiel.

{{% notice warning %}}
Offiziell wird von der Verwendung von "DinD" in einem CI-Workflow abgeraten.
Zu den Gründen für und wider siehe [hier](https://jpetazzo.github.io/2015/09/03/do-not-use-docker-in-docker-for-ci/).
{{% /notice %}}

[Docker-in-Docker](https://hub.docker.com/_/docker/) erlaubt es, wie der Name
schon sagt, das Betreiben von Docker-Containers innerhalb eines Containers.
Ein basales Setup in `.gitlab-ci.yml` sieht folgendermassen aus
([Quelle](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html)):

```yaml
image: docker:stable
 
  variables:
    # When using dind service we need to instruct docker, to talk with the
    # daemon started inside of the service. The daemon is available with
    # a network connection instead of the default /var/run/docker.sock socket.
    #
    # The 'docker' hostname is the alias of the service container as described at
    # https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#accessing-the-services
    #
    # Note that if you're using Kubernetes executor, the variable should be set to
    # tcp://localhost:2375 because of how Kubernetes executor connects services
    # to the job container
    DOCKER_HOST: tcp://docker:2375/
    # When using dind, it's wise to use the overlayfs driver for
    # improved performance.
    DOCKER_DRIVER: overlay2
 
  services:
    - docker:dind
 
# ...
```

Auf dieser Grundlage sind eine Menge spannender Dinge denkbar, bspw. das
Erstellen von ad-hoc-Images oder das [pushen von Images nach Docker
Hub]({{< ref "deployment-docker-hub.md" >}}). Doch möchte ich mich hier auf das Aufsetzen
von `docker-compose` beschränken.

Das `docker`-Image beruht auf Alpinelinux 3.8. In dieser Version ist
`docker-compose` noch nicht in den offiziellen Paketquellen enthalten, somit
müssen wir die Applikation via `pip` installieren (`docker-compose` ist in
Python geschrieben). Um das Aufsetzen des CI-Workflows von seinen eigentlichen
Aufgaben zu trennen, tun wir das mithilfe des `before_script`-Schlüsselwortes:

```yaml
# ...
  docker-compose-test:
    before_script:
      - apk update
      - apk add --no-cache py-pip # Installieren von pip via dem Paketmanager von Alpine
      - pip install docker-compose
```

Anschliessend bleibt als einziges noch zu tun, `docker-compose` aufzustarten.
Vorausgesetzt, `docker-compose.yml` befindet sich im root-Verzeichnis:

```yaml
# ...
  docker-compose-test:
    # ...
    script:
      - docker-compose up
```

Was schlussendlich zu folgendem `.gitlab-ci.yml` führt:

```yaml
image: docker:stable
  variables:
    DOCKER_HOST: tcp://docker:2375/
    DOCKER_DRIVER: overlay2

  services:
    - docker:dind

  docker-compose-test:
    before_script:
      - apk update
      - apk add --no-cache py-pip # Installieren von pip via dem Paketmanager von Alpine
      - pip install docker-compose
    script:
      - docker-compose up
```

{{% notice note %}}
Das Beispiel ist selbstverständlich etwas unterkomplex. Bei einem
realistischeren Ansatz müssten sich Fragen gestellt werden wie nach dem
korrekten Herunterfahren von in `docker-compose.yml` definierten Services, der
Reihenfolge beim Aufstarten etc.
{{% /notice %}}
