---
title: "Deploy Repository to Docker Hub"
date: 2019-01-22T11:29:00+01:00
anchor: "deployment-docker-hub"
---

In order to automatically deploy a Gitlab repository to [Docker
Hub](https://hub.docker.com), you have to perform the following steps:

* Set up some user-defined [CI
  variables](https://docs.gitlab.com/ee/ci/variables)
* Provide a `.gitlab-ci.yml` configuration

## Variables

Before we write the `.gitlab-ci.yml` file, we already define the variables
which are needed to connect to Docker Hub. These variables are defined
in the repository settings under `CI/CD` -> `Environment variables` and should
be the following:

* `CI_REGISTRY`: `index.docker.io`
* `CI_REGISTRY_NAME`: Docker hub user / organisation and name of repository,
  e.g. `swissbib/resource-transformer`
* `CI_REGISTRY_PASSWORD`: Docker hub password
* `CI_REGISTRY_USER`: Docker hub user

Don't protect the variables!

## CI configuration file

The deploy stage in `.gitlab-ci.yml` is based on the Docker image `docker:stable` as well as the image `docker:dind`. Its only task is to push the image to the docker hub (for which it obviously needs the respective credentials). 

```YAML
build-image:
  image: docker:stable
  services:
    - docker:dind
  script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD"
    - docker build --pull -t "$IMAGE_TAG" .
    - docker push "$IMAGE_TAG"
    - docker logout
```
