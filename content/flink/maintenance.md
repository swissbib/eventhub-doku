---
title: "Unterhalt"
date: 2020-04-06T07:42:00+01:00
anchor: "flink-maintenance"
---

## Allgemeine Hinweise

Restart des Flink-Clusters.

1. Zum root-Verzeichnis des Flink _job manager_ (master) navigieren
2. Cluster vollständig herunterfahren: `bin/stop-cluster.sh`
3. Cluster start: `bin/start-cluster.sh`

Alternativ lassen sich auch einzelne _task managers_ (workers)
stoppen/starten:

1. Zum root-Verzeichnis des betreffenden _task manager_ navigieren
2. _Task manager_ stoppen: `bin/taskmanager.sh stop`; _task manager_ starten:
   `bin/taskmanager.sh start`
