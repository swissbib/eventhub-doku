---
title: "Übersicht baseline (Stand 28.2.19)"
date: 2019-02-28T10:48:00+01:00
anchor: "overview-baseline"
---

![Übersicht baseline](/eventhub-doku/img/lsb-baseline.png?width=500px)

Folgende Änderungen sind vorgesehen:

* Anpassen der Topicnamen `person`, `organisation`, `item`, `resource` und
  `document`, um Ambiguitäten zu vermeiden
* `person`, `organisation` werden nicht mehr direkt von
  `elastic-bulk-consumer` konsumiert, sondern in die Anreicherungspipeline
geschickt
* `work-enriched` wird voraussichtlich nicht von `elastic-bulk-consumer`
  konsumiert werden, sondern benötigt einen spezialisierten _consumer_
