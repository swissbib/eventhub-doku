---
title: "Befehle für Kafka"
date: 2019-01-18T13:40:31+01:00
---


#### Topics
##### List topics
```sh
bin/kafka-topics.sh --list \
                    --zookeeper $ZK_HOST
```

##### Create topic
```sh
bin/kafka-topics.sh --create \
                    --zookeeper $ZK_HOST \
                    --partitions 4 \
                    --replication-factor 1 \
                    --topic test 
```

##### Delete topic
```sh
bin/kafka-topics.sh --delete \
                    --zookeeper $ZK_HOST \
                    --topic test
```

##### Describe topic
```sh
bin/kafka-topics.sh --describe \
                    --zookeeper $ZK_HOST \
                    --topic test
```

##### Anzahl Messages pro Topic:Partition
```sh
bin/kafka-run-class.sh kafka.tools.GetOffsetShell \
                       --broker-list localhost:9092 \
                       --time -1 \
                       --topic test 
```

#### Producer
##### Publish data over console
```sh
bin/kafka-console-producer.sh --broker-list 127.0.0.1:9092 \
                              --topic test
```


##### Publish data with key,value
```sh
bin/kafka-console-producer.sh --broker-list 127.0.0.1:9092 \
                              --property parse.key=true \
                              --property key.separator=, \
                              --topic test 
```

`parse.key` ensures that the provided string is parsed for key. Subsequently a valid string will be in the form of `key,value`.

#### Consumer
##### Consume data
```sh
bin/kafka-console-consumer.sh --bootstrap-server 127.0.0.1:9092 \
                              --topic test
```


##### Consume data from beginning
```sh
bin/kafka-console-consumer.sh --bootstrap-server 127.0.0.1:9092 \
                              --from-beginning \
                              --topic test 
```


##### Consume data from beginning and from specific partition
```sh
bin/kafka-console-consumer.sh --bootstrap-server 127.0.0.1:9092 \
                              --from-beginning \
                              --partition 1 \
                              --topic test 
```


##### Consume from specific group
```sh
bin/kafka-console-consumer.sh --bootstrap-server 127.0.0.1:9092 \
                              --topic test \
                              --consumer-property group.id=mygroupid
```

If a group id is designated, offsets will automatically be committed to Kafka. Thus a reconnected consumer will never show every message even with the `--from-beginning` flag.
