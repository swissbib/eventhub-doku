---
title: "Troubleshoot"
date: 2019-11-18T17:17:53+01:00
---

Problem: Can no longer access logs from worker node, because worker node is not available. But worker 
node is actually ready when checking with `docker node ls`.

```
docker swarm ca --rotate
```

This will redistribute the certificates and it should work again.
