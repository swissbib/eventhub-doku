---
title: "Troubleshoot Elastic"
date: 2019-01-18T13:40:31+01:00
---

## Disc runs full

When the discs runs full elastic will automatically change all index to read only. To change this increase 
disk space or delete some index to make space on disk.

Once enough space is there the all index must be unlocked again manually. This can be done with the following command:

```
curl -XPUT -H "Content-Type: application/json"    http://sb-ues8:8080/_all/_settings      -d '{"index.blocks.read_only_allow_delete": false}'
```