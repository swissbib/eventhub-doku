---
title: "Wikidata"
date: 2019-01-08T14:22:32+01:00
---

There are a number of ways to download Wikidata data. All these formats can be referenced here:

https://www.wikidata.org/wiki/Wikidata:Database_download

The dumps can be downloaded from here: https://dumps.wikimedia.org/wikidatawiki/entities/.

We use the `latest-truthy.nt.bz2` dumps. These contain all truthy statements. They are only 
a subset of the entire wikidata dataset. No qualified properties can be found. For example 
the population of a city may have several values, each with a qualifier date, of when that 
was the case. In this dump only the latest population will be used.

The decision on what is the most truthy statement depends on the type of property and qualifier.

To get the jsonld or n-triples representation of a data item use:

http://www.wikidata.org/wiki/Special:EntityData/Q42.nt
http://www.wikidata.org/wiki/Special:EntityData/Q42.json

Replace Q42 with the number of the entity you like to get.