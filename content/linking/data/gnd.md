---
title: "GND"
date: 2019-01-08T14:22:36+01:00
---

Die GND wird von http://lobid.org/gnd/search importiert. Aktuell gibt es hier noch eine bug, der verhindert,
dass die Daten vollständig sind. Sollte man mal melden!

Die Dokumentation der API kann hier gefunden werden: http://lobid.org/gnd/api

Die Daten kommen als JSON-LD Objekt pro Linie. Dies erlaubt es diese direkt zu importieren. Der Ablauf des Imports
wird durch das [GND-Update Script](https://gitlab.com/swissbib/linked/workflows/tree/master/gnd) ausgeführt. Dieses Skript wird durch einen cron-Job einmal im Monat ausgeführt.
Das Skript kann aber auch manuell gestartet werden. 

Vergleich die README Datei für detailierte instruktionen!
