---
title: "Viaf"
date: 2019-01-08T14:22:22+01:00
---


## VIAF dumps

The VIAF dumps are available from https://viaf.org/viaf/data/. The data is released monthly and in seven different formats.

The format we use is the RDF/XML-Clusters file. This is the easiest to process and transfrom into JSON-LD.

### Example Link

http://viaf.org/viaf/data/viaf-20190107-clusters-rdf.xml.gz

It is important to not take the clusters.xml file without the rdf. This data is not valid RDF, but some kind of native serialization / data model.

In order to download the latest set, the date in the link has to be changed. This means that it would be easiest to check monthly to when to update the VIAF
data.