---
title: "DBpedia"
date: 2019-01-08T14:22:26+01:00
---

### DBpedia Dumps

The DBpedia dumps are available from http://downloads.dbpedia.org/. All old dumps and new dumps are available. The newer dumps can be found under
http://downloads.dbpedia.org/repo/lts/mappings/:

- **instance-types**: Contains the rdf:types triples for all objects in dbpedia. This can be used to search for persons and organisations. The persons are defined with `foaf:Person` and the organisations with `schema:Organisation`.
- **instance-types-transitive**: Contains all super types of the basic types.
- **mappingbased-literals**: All triples with a string literal as object. Here most of the data can be found which we need (rdfs:comment, dpo:birthDate, dpo:deathDate etc.)
- **mappingbased-objects-uncleaned**: All triples with a IRI as object. Here most of the links between the data can be found, such as connected authors etc.
- **specific-mappingbased-properties**: Triples with numerical, boolean etc. type objects. These are not used.
- **geo-coordinates-mappingbased**: Contains all triples with geographic values. Currently not used in swissbib and is not needed.

Each folder contains sub-folders with dates, which are the dates when these dumps were generated. New dumps come every 2 - 4 weeks. Each time a new dump is found the dump can be downloaded with the script wrapped inside a docker container:

[https://gitlab.com/swissbib/linked/linking/dbpedia-download](https://gitlab.com/swissbib/linked/linking/dbpedia-download)

**Publikation der DBpedia Daten neu auf [Databus](http://databus.dbpedia.org)!**

The global ID Clusters can be downloaded here: https://downloads.dbpedia.org/repo/dev/global-id-management/sameas-clusters/2018.11.09/
It is however missing the actual global IDs. Where can I get them except for the service? http://dev.dbpedia.org/Global_IRI_Resolution_Service

The developer documentation can be found here: http://dev.dbpedia.org/

To work with the databus data the triples for the resources have to be collected first. This is easiest with

```
bzcat <files> | sort -T <temp-location> | bzip2 > <merged-file-location>
```

http://downloads.dbpedia.org/live/abstracts/changesets-abstracts/