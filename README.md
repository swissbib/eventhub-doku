# Interne Dokumentation Event Hub

Hallo!

## Verbessere mich

Eine Dokumentation kann immer noch besser werden. Deshalb sind Verbesserungs- und Ergänzungsvorschläge via [Issues](https://gitlab.com/swissbib/eventhub-doku/issues) und [Pull Requests](https://gitlab.com/swissbib/eventhub-doku/pulls) sind herzlich willkommen! 

Es ist auch möglich, einen PR direkt von der gerenderten Seite zu
starten. Dazu einfach auf das kleine graue "Branch"-Logo ganz unten rechts
klicken!

## Lokale Installation

Um die Dokumentation lokal anzeigen zu lassen, sind folgende Schritte notwendig:

0. [Hugo](https://gohugo.io/) installieren
1. Terminal starten und Repository _rekursiv_ klonen: `git clone
   --recursive
   https://gitlab.com/swissbib/eventhub-doku`
2. In das neu erstellte Verzeichnis wechseln: `cd eventhub-doku`
3. Hugo-Server starten: `hugo server`
4. Doku im Browser mit `localhost:1313` aufrufen
